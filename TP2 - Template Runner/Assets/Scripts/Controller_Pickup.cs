using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Pickup : MonoBehaviour
{

    [SerializeField] Image gasLineImg;
    public float gasLeft;
    public float maxGas = 15;
    
    void Start()
    {
        RefillGas();
    }

    void Update()
    {
        if (gasLeft > 0) //Gasolina
        {
            gasLeft -= Time.deltaTime;
            gasLineImg.fillAmount = gasLeft / maxGas;
        }
    }

    public void RefillGas() 
    {
        gasLeft = maxGas;
    
    }
}
