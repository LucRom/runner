using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Instatiator_Rocket : MonoBehaviour
{
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public float respawningTimer;
    public float time = 0;
    public bool rocketUpWarning = false;
    public bool rocketMidWarning = false;

    void Start()
    {
        Controller_Rocket.rocketVelocity = 1;
    }

    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Rocket.rocketVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;
        GameObject rocket = null;

        if (respawningTimer <= 0)
        {
            
            rocket = Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(10, 20);

        }
        if(rocket!= null)
        {
            GestorDeAudio.instancia.ReproducirSonido("RocketWarning Sfx");
            if (rocket.name == "Rocket Mid(Clone)")
            {
                rocketMidWarning = true;
            }
            else if (rocket.name == "Rocket Up(Clone)")
            {
                rocketUpWarning = true;
            }
        }
    }
}
