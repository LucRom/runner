using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.ProBuilder;
using UnityEngine.ProBuilder.Shapes;

public class Object_Pooling : MonoBehaviour
{
    public List<GameObject> enemies;
    private List<GameObject> pooledEnemies;
    public ObjectPool<Controller_Enemy> enemiesPool;
    private GameObject enemyToPool;
    public GameObject poolPos;
    public float respawningTimer;
    public float time = 0;

    private void Awake()
    {
    }
    void Start()
    {
        Controller_Enemy.enemyVelocity = 1;
    }
    private void Update()
    {
        SpawnEnemies();
    }
    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            respawningTimer = UnityEngine.Random.Range(2, 6);
        }
    }
}
