﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public List<GameObject> pickups;
    public GameObject instantiatePos;
    public float respawningTimer;
    public float respawningTimerPickUp;
    public float time = 0;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 1;
    }

    void Update()
    {
        SpawnItems();
        SpawnEnemies();
        ChangeVelocity();
    }

    private void ChangeVelocity()
    {
        //time += Time.deltaTime;
        //Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 8);
        }
    }

    private void SpawnItems() 
    {
        respawningTimerPickUp -= Time.deltaTime;

        if (respawningTimerPickUp <= 0)
        {
            Instantiate(pickups[UnityEngine.Random.Range(0, pickups.Count)], instantiatePos.transform);
            respawningTimerPickUp = UnityEngine.Random.Range(2, 4);
        }
    }
}
