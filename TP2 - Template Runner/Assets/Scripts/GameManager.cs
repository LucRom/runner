using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool dead;
    public float distance = 0;
    public int recordDistance = 0;
    public GameObject flag;
    [SerializeField] GameObject player;
    [SerializeField] GameObject instatiator;
    [SerializeField] GameObject instatiator_rocket;

    Controller_Instatiator_Rocket rockets;
    Controller_Instantiator controller_instatiator;
    Controller_Pickup myPickupController;

    void Start()
    {
        recordDistance = GestorPersistencia.instancia.CargarRecord();
        flag.SetActive(false);
        GestorDeAudio.instancia.ReproducirSonido("Jetpack Joyride Main Theme");
        rockets = GameObject.Find("Instantiator_Rocket").GetComponent<Controller_Instatiator_Rocket>();
        controller_instatiator = instatiator.GetComponent<Controller_Instantiator>();
        myPickupController = GameObject.Find("Pickup Controller").GetComponent<Controller_Pickup>();
        dead = false;
    }
    void Update()
    {
        if (dead)
        {
            Time.timeScale = 0;
            player.SetActive(false);
            GestorDeAudio.instancia.PausarSonido("Jetpack Joyride Main Theme");
        }
        else
        {
            distance += Time.deltaTime;
            if(distance > recordDistance)
            {
                recordDistance = Convert.ToInt32(distance);
                GestorPersistencia.instancia.GuardarRecord();
                flag.SetActive(true);
            }

            if (myPickupController.gasLeft < 0)
            {
                dead = true;
                Controller_Hud.gameOver = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            StartGame();
        }
    }
    private void StartGame()
    {
        recordDistance = GestorPersistencia.instancia.CargarRecord();
        GestorPersistencia.instancia.CargarDataPersistencia();
        flag.SetActive(false);
        GestorDeAudio.instancia.ReproducirSonido("Jetpack Joyride Main Theme");
        controller_instatiator.time = 0;
        rockets.time = 0;
        myPickupController.RefillGas();
        player.transform.position = new Vector3(-4, 0.1f, 0f);
        Controller_Hud.gameOver = false;
        player.SetActive(true);
        distance = 0;
        dead = false;
        Time.timeScale = 1;
        for (var i = instatiator.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(instatiator.transform.GetChild(i).gameObject);
        }
        for (var i = instatiator_rocket.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(instatiator_rocket.transform.GetChild(i).gameObject);
        }
        rockets.rocketMidWarning= false;
        rockets.rocketUpWarning = false;
    }
    public void BorrarDistanciaRecord()
    {
        recordDistance = 0;
        GestorPersistencia.instancia.BorrarRecord();
    }
}
