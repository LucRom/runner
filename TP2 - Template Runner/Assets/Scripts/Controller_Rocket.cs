using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class Controller_Rocket : MonoBehaviour
{
    public static float rocketVelocity;
    private Rigidbody rb;
    Controller_Instatiator_Rocket rockets;

    void Start()
    {
        rockets = GameObject.Find("Instantiator_Rocket").GetComponent<Controller_Instatiator_Rocket>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb.AddForce(new Vector3(-rocketVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
            rockets.rocketMidWarning = false;
            rockets.rocketUpWarning = false;
        }
    }
    public void DestroytThisEnemy()
    {
        Destroy(this.gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }
}
