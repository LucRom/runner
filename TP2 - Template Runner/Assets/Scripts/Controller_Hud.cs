﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public Text recordDistanceText;
    public GameObject warnignUp;
    public GameObject warnignMid;
    private int nuevaDistancia;
    GameManager manager;
    Controller_Instatiator_Rocket rockets;

    void Start()
    {
        manager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        rockets = GameObject.Find("Instantiator_Rocket").GetComponent<Controller_Instatiator_Rocket>();
        warnignMid.SetActive(false);
        warnignUp.SetActive(false);
        gameOver = false;
        manager.distance = 0;
        distanceText.text = manager.distance.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + nuevaDistancia.ToString() + "Mts \n Record: " +manager.recordDistance.ToString()+"Mts";
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            gameOverText.text = "";
            nuevaDistancia = Convert.ToInt32(manager.distance);
            distanceText.text = nuevaDistancia.ToString()+" Mts";
            recordDistanceText.text = "Record: " + manager.recordDistance.ToString();
        }
        if (!rockets.rocketMidWarning)
        {
            warnignMid.SetActive(false);
        }
        if (rockets.rocketMidWarning)
        {
            warnignMid.SetActive(true);
        }
        if(!rockets.rocketUpWarning)
        {
            warnignUp.SetActive(false);
        }
        if (rockets.rocketUpWarning)
        {
            warnignUp.SetActive(true);
        }
    }
}
