using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GestorPersistencia : MonoBehaviour
{
    public static GestorPersistencia instancia;
    public DataPersistencia data;
    string archivoDatos = "save.dat";

    private void Awake()
    {
        if (instancia == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
            Destroy(this.gameObject);

        CargarDataPersistencia();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G)) GuardarDataPersistencia();
    }
    public void GuardarRecord()
    {
        data.record = GameObject.Find("Game Manager").GetComponent<GameManager>().recordDistance;
        GuardarDataPersistencia();
    }
    public int CargarRecord()
    {
        return data.record;
    }
    public void BorrarRecord()
    {
        data.record = 0;
        GuardarDataPersistencia();
    }

    public void GuardarDataPersistencia()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Datos guardados");
    }

    public void CargarDataPersistencia()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;
            file.Close();
            Debug.Log("Datos cargados");
        }
    }
}
[System.Serializable]
public class DataPersistencia
{
    public int record = 0;
    public DataPersistencia()
    {
        record = 0;
    }
}

[System.Serializable]
public class Punto
{
    public float x;
    public float y;
    public float z;

    public Punto(Vector3 p)
    {
        x = p.x; y = p.y; z = p.z;
    }

    public Vector3 aVector()
    {
        return new Vector3(x, y, z);
    }
}


