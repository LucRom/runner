﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    Controller_Input controller_Input;
    GameManager manager;

    private void Start()
    {
        manager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        controller_Input = GetComponent<Controller_Input>();
    }

    void Update()
    {
        controller_Input.GetInput();
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Controller_Hud.gameOver = true;
            manager.dead = true;
        }
        GestorDeAudio.instancia.PausarSonido("Minigun Sfx");
    }
}
