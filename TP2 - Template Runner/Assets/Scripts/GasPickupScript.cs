using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasPickupScript : MonoBehaviour
{
    static float itemVelocity = 0.8f;
    private Rigidbody rb;
    Controller_Pickup myPickupController; 


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        myPickupController = GameObject.Find("Pickup Controller").GetComponent<Controller_Pickup>();
    }

    void Update()
    {
        rb.AddForce(new Vector3(-itemVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Gas fill up Sfx");
            myPickupController.gasLeft = myPickupController.maxGas;
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }

}
