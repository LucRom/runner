using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Input : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    public GameObject bullet;
    [SerializeField] GameObject zona1;
    [SerializeField] GameObject zona2;
    GameManager manager;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        manager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }
    void Update()
    {

    }
    public void GetInput()
    {
        Jump();
        ResetRecord();
    }
    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            GestorDeAudio.instancia.ReproducirSonido("Minigun Sfx");
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            GameObject bullet1;
            bullet1 = Instantiate(bullet, zona1.transform.position, Quaternion.identity);
            Rigidbody bullet1RB = bullet1.GetComponent<Rigidbody>();
            bullet1RB.AddForce(Vector3.down * 15f, ForceMode.Impulse);
            GameObject bullet2;
            bullet2 = Instantiate(bullet, zona2.transform.position, Quaternion.identity);
            Rigidbody bullet2RB = bullet2.GetComponent<Rigidbody>();
            bullet2RB.AddForce(Vector3.down * 15f, ForceMode.Impulse);
        }
    }
    private void ResetRecord()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            manager.BorrarDistanciaRecord();
        }
    }

}
